<?php
Intechsoft
/**
 * Class Intechsoft_Youtube_Block_Player
 */
class Intechsoft_Youtube_Block_Player extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    /**
     * @return string
     */
    protected function _toHtml() {
        $videoId = $this->getVideoId($this->getYoutubeLink());
        if ((int)$this->getPlayerWidth() != 0 && $this->getPlayerWidth() != '100%'){
            $playerWidth = (int)$this->getPlayerWidth();
        } else if ($this->getPlayerWidth() == '100%') {
            $playerWidth = '100%';
        } else {
            $playerWidth = 640;
        }

        if ((int)$this->getPlayerHeight() != 0){
            $playerHeight = (int)$this->getPlayerHeight();
        } else {
            $playerHeight = 390;
        }

        $customConfigParams = $this->getCustomConfig($this->getCustomConfigParams());

        $html = "<div id='player'></div>

    <script>
      var tag = document.createElement('script');

      tag.src = 'https://www.youtube.com/iframe_api';
      var firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

      var player;
      function onYouTubeIframeAPIReady() {
        player = new YT.Player('player', {
          height: '".$playerHeight."',
          width: '".$playerWidth."',
          videoId: '". $videoId."',
          playerVars: {".$customConfigParams."},
          events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
          }
        });
      }

      function onPlayerReady(event) {
        event.target.playVideo();
      }

      var done = false;
      function onPlayerStateChange(event) {
          if (event.data == YT.PlayerState.PLAYING && !done) {
              setTimeout(stopVideo, 6000);
              done = true;
          }
      }
      function stopVideo() {
          player.stopVideo();
      }
    </script>";

        return $html;
    }

    /**
     * @param $youtubeLink
     * @return string
     */
    protected function getVideoId($youtubeLink)
    {
        if  (strpos($youtubeLink, 'http') === false ){
            $videoId = $youtubeLink;
        } else {
            $arr = explode('=',$youtubeLink);
            $videoId = end($arr);
        }
        return $videoId;
    }

    /**
     * @param $params
     * @return string
     */
    protected function getCustomConfig($params)
    {
        return trim($params);
    }
}
